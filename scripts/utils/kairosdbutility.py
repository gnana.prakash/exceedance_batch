"""
Utility for Querying KairosDB
__author__ = 'H115-125'
"""
# -----------------Start of Import statements------------------------ #
import requests
# from requests.packages.urllib3.util.retry import Retry
request_input = requests.Session()
from scripts.logging.Logging import logger as log
# -----------------end of Import statements------------------------ #


class KairosDButlity(object):

    """
    This class is to KairosDButlity
    """

    def __init__(self, data_url):
        """
           Initializer
        """
        self.data_url = data_url
        self.success_code = [204, 200]
        self.insert_update_url = "/api/v1/datapoints"
        self.avg_query_url = "/api/v1/datapoints/query"
        self.delete_url = "/api/v1/datapoints/delete"
        self.delete_metric_url = "/api/v1/metric/"

    def insert_data(self, metric_json):
        """
                   This function is to inserting data
                   :param metric_json:
                   :return :
        sample format of metric_json
        metric_json = [
              {
                  "name": "glens.upload.valid",
                  # "timestamp": 1531876658,
                  # "value": 123.4,
                  "datapoints": data_list,
                  "tags": {
                      'site_id': 'site_71',
                      'state': 'andhra_pradesh',
                      'city': 'kadapa',
                      'industry_type': 'cement',
                      'monitoring_type': 'Emission',
                      'monitoring_id': 'RAW_MILL_KILN',
                      'analyser_id': 'analyzer_8',
                      'parameter_id': 'parameter_10',
                      'quality': 'U'
                  }
              }
        ]
        """
        try:
            url = self.data_url + self.insert_update_url
            resp = requests.post(url, json=metric_json)
            if resp.status_code not in self.success_code:
                log.info("Inserting data  failed")
                log.warn(resp.text)
                return False
            else:
                log.info("data inserted successfully")
                return True
        except Exception as e:
            log.exception("Exception while inserting the data " + str(self.data_url) + str(
                e))
            return False

    def fetch_data(self, metric_json, from_time, to_time):
        """
               This function is to fetch data
               :param metric_json:
               :param from_time:
               :param to_time:
               :return :
        sample format of metric_json
        metric_json = [
              {
                  "name": "glens.upload.valid",
                  "tags": {
                      'site_id': 'site_71',
                      'monitoring_type': 'Emission',
                      'monitoring_id': 'RAW_MILL_KILN',
                      'analyser_id': 'analyzer_8',
                      'parameter_id': 'parameter_10',
                      'quality': 'U'
                  }
              }
        ]
        """
        try:
            url = self.data_url + self.avg_query_url
            request_data = {"metrics": metric_json, "cache_time": 0, "start_absolute": from_time,
                            "end_absolute": to_time}
            resp = requests.post(url, json=request_data)
            if resp.status_code not in self.success_code:
                log.info("data fetch failed")
                return False
            else:
                print("data fetched successfully")
                return resp.json()
        except Exception as e:
            log.exception("Exception while fetching the data " + str(self.data_url) + str(
                e))
            return False

    def delete_data(self, metric_json, from_time, to_time):
        """
                This function is to delete data
                :param metric_json:
                :param from_time:
                :param to_time:
                :return :
        sample format of metric_json
        metric_json = [
              {
                  "name": "glens.upload.valid",
                  "tags": {
                      'site_id': 'site_71',
                      'monitoring_type': 'Emission',
                      'monitoring_id': 'RAW_MILL_KILN',
                      'analyser_id': 'analyzer_8',
                      'parameter_id': 'parameter_10',
                      'quality': 'U'
                  }
              }
        ]
        """
        try:
            url = self.data_url + self.delete_url
            request_data = {"metrics": metric_json, "cache_time": 0, "start_absolute": from_time,
                            "end_absolute": to_time}
            # request_input.mount(url, HTTPAdapter(max_retries=5))
            # resp = request_input.post(url,json=request_data)
            resp = requests.post(url, json=request_data)
            if resp.status_code not in self.success_code:
                log.info("error in deleting data")
                log.warn(resp.text)
                return False
            else:
                log.info("data deleted successfully")
                return True
        except Exception as e:
            log.exception("Exception While deleting the data " + str(self.data_url) + str(
                e))
            return False

    def delete_metric(self, metric_name):
        """
                This function is to delete matrix
                :param metric_name:
                :return :
         """
        try:
            url = self.data_url + self.delete_metric_url+"{" + str(metric_name) + "}"
            resp = requests.delete(url)
            if resp.status_code not in self.success_code:
                log.info("error in deleting metric")
                log.warn(resp.text)
                return False
            else:
                log.info("metric deleted successfully")
                return True
        except Exception as e:
            log.exception("Exception While deleting the metric " + str(self.data_url) + str(
                e))
            return False

    def fetch_avg_data(self, metric_json, from_time, to_time):
        """
               This function is to fetch avg data
               :param metric_json:
               :param from_time:
               :param to_time:
               :return :
        sample format of metric_json
        metric_json = [
              {
                  "name": "glens.upload.valid",
                  "tags": {
                      'site_id': 'site_71',
                      'monitoring_type': 'Emission',
                      'monitoring_id': 'RAW_MILL_KILN',
                      'quality': 'U'
                  },
                  "aggregators": [{
                      "name": "avg",
                      "align_sampling": True,
                      "sampling": {
                         "value": 1,
                         "unit": "minutes"
                      }
                  }],
                  "group_by": [{
                        "name": "tag",
                        "tags": ["monitoring_id", "analyser_id", "parameter_id"]
                  }]

              }
        ]
        """
        try:
            url = self.data_url + self.avg_query_url
            request_data = {"metrics": metric_json, "cache_time": 0, "start_absolute": from_time,
                            "end_absolute": to_time}
            print(10*'*' + "Started hitting the Kairos database with the json " + 10*'*')
            resp = request_input.post(url, json=request_data)
            log.debug("Status code for the post request: %s", resp.status_code)
            if resp.status_code not in self.success_code:
                log.info("Avg data fetch failed")
                log.warn(resp.text)
                return False
            else:
                log.info("Avg data fetched successfully")
                return resp.json()
        except Exception as e:
            log.exception("Exception while fetching the Avg data " + str(self.data_url) + str(
                e))
            return False

    def update_data(self, metric_json):
        """
                   This function is to update data
                   :param metric_json:
                   :return :
        sample format of metric_json
        metric_json = [
              {
                  "name": "glens.upload.valid",
                  "timestamp": 1527791700,
                  "value": 8888.88,
                  # "datapoints": [[1527791700,8888.88]],
                  "tags": {
                      'site_id': 'site_71',
                      'monitoring_type': 'Emission',
                      'monitoring_id': 'RAW_MILL_KILN',
                      'analyser_id': 'analyzer_8',
                      'parameter_id': 'parameter_10',
                  }
              }
        ]
        """
        try:
            url = self.data_url + self.insert_update_url
            resp = requests.post(url, json=metric_json)
            if resp.status_code not in self.success_code:
                log.info("Updating data failed")
                log.warn(resp.text)
                return False
            else:
                log.info("data updated successfully")
                return True
        except Exception as e:
            log.exception("Exception while Updating the data " + str(self.data_url) + str(
                e))
            return False
