"""
Forming Json for KairosDB
__author__ = 'H115-125'
"""
# -----------------Start of Import statements------------------------ #
from scripts.logging.Logging import logger as log
# -----------------end of Import statements------------------------ #


class KairosDBJson(object):

    """
    This class is to KairosDBJson
    """

    def __init__(self):
        """
           Initializer
        """

    @staticmethod
    def insert_data_point_json(metric_name, date_time, value, **kwargs):
        """
                This function is  to form a json for data point
                :param metric_name:
                :param date_time:
                :param value:
                :param kwargs:
                :return :
        """
        try:
            tags_json = dict()
            if "site_id" in kwargs and kwargs["site_id"] is not None:
                tags_json["site_id"] = kwargs["site_id"]
            if "state" in kwargs and kwargs["state"] is not None:
                tags_json["state"] = kwargs["state"]
            if "city" in kwargs and kwargs["city"] is not None:
                tags_json["city"] = kwargs["city"]
            if "industry_type" in kwargs and kwargs["industry_type"] is not None:
                tags_json["industry_type"] = kwargs["industry_type"]
            if "monitoring_type" in kwargs and kwargs["monitoring_type"] is not None:
                tags_json["monitoring_type"] = kwargs["monitoring_type"]
            if "monitoring_id" in kwargs and kwargs["monitoring_id"] is not None:
                tags_json["monitoring_id"] = kwargs["monitoring_id"]
            if "analyzer_id" in kwargs and kwargs["analyzer_id"] is not None:
                tags_json["analyser_id"] = kwargs["analyzer_id"]
            if "parameter_id" in kwargs and kwargs["parameter_id"] is not None:
                tags_json["parameter_id"] = kwargs["parameter_id"]
            if "quality_code" in kwargs and kwargs["quality_code"] is not None:
                tags_json["quality"] = kwargs["quality_code"]

            metric_json = [
                {
                    "name": metric_name,
                    "timestamp": date_time,
                    "value": value,
                    "tags": tags_json
                }
            ]
            log.debug("metric_json")
            log.debug(metric_json)
            return metric_json
        except Exception as e:
            log.exception("Exception while forming json for inserting a data point " + str(e))
            return False

    @staticmethod
    def insert_data_list_json(metric_name, data_list, **kwargs):
        """
                This function is  to form a json for data_list
                :param metric_name:
                :param data_list:
                :param kwargs:
                :return :
        """
        try:
            tags_json = dict()
            if "site_id" in kwargs and kwargs["site_id"] is not None:
                tags_json["site_id"] = kwargs["site_id"]
            if "state" in kwargs and kwargs["state"] is not None:
                tags_json["state"] = kwargs["state"]
            if "city" in kwargs and kwargs["city"] is not None:
                tags_json["city"] = kwargs["city"]
            if "industry_type" in kwargs and kwargs["industry_type"] is not None:
                tags_json["industry_type"] = kwargs["industry_type"]
            if "monitoring_type" in kwargs and kwargs["monitoring_type"] is not None:
                tags_json["monitoring_type"] = kwargs["monitoring_type"]
            if "monitoring_id" in kwargs and kwargs["monitoring_id"] is not None:
                tags_json["monitoring_id"] = kwargs["monitoring_id"]
            if "analyzer_id" in kwargs and kwargs["analyzer_id"] is not None:
                tags_json["analyser_id"] = kwargs["analyzer_id"]
            if "parameter_id" in kwargs and kwargs["parameter_id"] is not None:
                tags_json["parameter_id"] = kwargs["parameter_id"]
            if "quality_code" in kwargs and kwargs["quality_code"] is not None:
                tags_json["quality"] = kwargs["quality_code"]

            metric_json = [
                {
                    "name": metric_name,
                    "datapoints": data_list,
                    "tags": tags_json
                }
            ]
            log.debug("metric_json")
            log.debug(metric_json)
            return metric_json
        except Exception as e:
            log.exception("Exception while forming json for inserting data list " + str(e))
            return False

    @staticmethod
    def data_json(metric_name, **kwargs):
        """
                This function is  to form a json to fetch/delete
                :param metric_name:
                :param kwargs:
                :return :
        """
        try:
            tags_json = dict()
            if "site_id" in kwargs and kwargs["site_id"] is not None:
                tags_json["site_id"] = kwargs["site_id"]
            if "state" in kwargs and kwargs["state"] is not None:
                tags_json["state"] = kwargs["state"]
            if "city" in kwargs and kwargs["city"] is not None:
                tags_json["city"] = kwargs["city"]
            if "industry_type" in kwargs and kwargs["industry_type"] is not None:
                tags_json["industry_type"] = kwargs["industry_type"]
            if "monitoring_type" in kwargs and kwargs["monitoring_type"] is not None:
                tags_json["monitoring_type"] = kwargs["monitoring_type"]
            if "monitoring_id" in kwargs and kwargs["monitoring_id"] is not None:
                tags_json["monitoring_id"] = kwargs["monitoring_id"]
            if "analyzer_id" in kwargs and kwargs["analyzer_id"] is not None:
                tags_json["analyser_id"] = kwargs["analyzer_id"]
            if "parameter_id" in kwargs and kwargs["parameter_id"] is not None:
                # tags_json["parameter_id"] = kwargs["parameter_id"]
                tags_json["parameter_id"] = [kwargs["parameter_id"]]
                if kwargs["parameter_id"] in ["parameter_3", "parameter_10"]:
                    tags_json["parameter_id"] = ["parameter_3", "parameter_10"]
                if kwargs["parameter_id"] in ["parameter_318", "parameter_12"]:
                    tags_json["parameter_id"] = ["parameter_318", "parameter_12"]
            if "quality_code" in kwargs and kwargs["quality_code"] is not None:
                tags_json["quality"] = kwargs["quality_code"]

            metric_json = [
                {
                    "name": metric_name,
                    "tags": tags_json
                }
            ]
            log.debug("metric_json")
            log.debug(metric_json)
            return metric_json
        except Exception as e:
            log.exception("Exception while forming json for inserting a data point " + str(e))
            return False

    @staticmethod
    def avg_data_json(metric_name, cal_avg, **kwargs):
        """
                This function is  to form a json to avg data
                :param metric_name:
                :param cal_avg:
                :param kwargs:
                :return :
        """
        try:
            tags_json = dict()
            if "site_id" in kwargs and kwargs["site_id"] is not None:
                tags_json["site_id"] = kwargs["site_id"]
            if "state" in kwargs and kwargs["state"] is not None:
                tags_json["state"] = kwargs["state"]
            if "city" in kwargs and kwargs["city"] is not None:
                tags_json["city"] = kwargs["city"]
            if "industry_type" in kwargs and kwargs["industry_type"] is not None:
                tags_json["industry_type"] = kwargs["industry_type"]
            if "monitoring_type" in kwargs and kwargs["monitoring_type"] is not None:
                tags_json["monitoring_type"] = kwargs["monitoring_type"]
            if "monitoring_id" in kwargs and kwargs["monitoring_id"] is not None:
                tags_json["monitoring_id"] = kwargs["monitoring_id"]
            if "analyzer_id" in kwargs and kwargs["analyzer_id"] is not None:
                tags_json["analyser_id"] = kwargs["analyzer_id"]
            if "parameter_id" in kwargs and kwargs["parameter_id"] is not None:
                # tags_json["parameter_id"] = kwargs["parameter_id"]
                tags_json["parameter_id"] = [kwargs["parameter_id"]]
                if kwargs["parameter_id"] in ["parameter_3", "parameter_10"]:
                    tags_json["parameter_id"] = ["parameter_3", "parameter_10"]
                if kwargs["parameter_id"] in ["parameter_318", "parameter_12"]:
                    tags_json["parameter_id"] = ["parameter_318", "parameter_12"]
            if "quality_code" in kwargs and kwargs["quality_code"] is not None:
                tags_json["quality"] = kwargs["quality_code"]
            sampling_json = dict()
            value = str(cal_avg).split("-")[0]
            units = str(cal_avg).split("-")[1]
            sampling_json["value"] = value
            sampling_json["unit"] = units
            metric_json = [
                {
                    "name": metric_name,
                    "tags": tags_json,
                    "aggregators": [{
                        "name": "avg",
                        "align_sampling": True,
                        "sampling": sampling_json
                    }]
                }
            ]
            log.debug("metric_json")
            log.debug(metric_json)
            return metric_json
        except Exception as e:
            log.exception("Exception while forming json for averaging data " + str(e))
            return False

    @staticmethod
    def avg_group_data_json(metric_name, cal_avg, **kwargs):
        """
                This function is  to form a json to avg data using group by
                :param metric_name:
                :param cal_avg:
                :param kwargs:
                :return :
        """
        try:
            tags_json = dict()
            tags_list = list()
            if "site_id" in kwargs and kwargs["site_id"] is not None:
                tags_json["site_id"] = kwargs["site_id"]
            if "state" in kwargs and kwargs["state"] is not None:
                tags_json["state"] = kwargs["state"]
            if "city" in kwargs and kwargs["city"] is not None:
                tags_json["city"] = kwargs["city"]
            if "industry_type" in kwargs and kwargs["industry_type"] is not None:
                tags_json["industry_type"] = kwargs["industry_type"]
            if "monitoring_type" in kwargs and kwargs["monitoring_type"] is not None:
                tags_json["monitoring_type"] = kwargs["monitoring_type"]
            else:
                tags_list.append("monitoring_type")
            if "monitoring_id" in kwargs and kwargs["monitoring_id"] is not None:
                tags_json["monitoring_id"] = kwargs["monitoring_id"]
            else:
                tags_list.append("monitoring_id")
            if "analyzer_id" in kwargs and kwargs["analyzer_id"] is not None:
                tags_json["analyser_id"] = kwargs["analyzer_id"]
            else:
                tags_list.append("analyser_id")
            if "parameter_id" in kwargs and kwargs["parameter_id"] is not None:
                # tags_json["parameter_id"] = kwargs["parameter_id"]
                tags_json["parameter_id"] = [kwargs["parameter_id"]]
                if kwargs["parameter_id"] in ["parameter_3", "parameter_10"]:
                    tags_json["parameter_id"] = ["parameter_3", "parameter_10"]
                if kwargs["parameter_id"] in ["parameter_318", "parameter_12"]:
                    tags_json["parameter_id"] = ["parameter_318", "parameter_12"]
            else:
                tags_list.append("parameter_id")
            if "quality_code" in kwargs and kwargs["quality_code"] is not None:
                tags_json["quality"] = kwargs["quality_code"]
            # tags_list = ["monitoring_id"]
            sampling_json = dict()
            value = str(cal_avg).split("-")[0]
            units = str(cal_avg).split("-")[1]
            sampling_json["value"] = value
            sampling_json["unit"] = units
            if tags_list is not None and tags_list != []:
                metric_json = [
                    {
                        "name": metric_name,
                        "tags": tags_json,
                        "aggregators": [{
                            "name": "avg",
                            "align_sampling": True,
                            "sampling": sampling_json
                        }],
                        "group_by": [{
                            "name": "tag",
                            "tags": tags_list
                        }]
                    }
                ]
            else:
                metric_json = [
                    {
                        "name": metric_name,
                        "tags": tags_json,
                        "aggregators": [{
                            "name": "avg",
                            "align_sampling": True,
                            "sampling": sampling_json
                        }]
                    }
                ]
            log.debug("metric_json")
            log.debug(metric_json)
            return metric_json
        except Exception as e:
            log.exception("Exception while forming json for averaging data " + str(e))
            return False
