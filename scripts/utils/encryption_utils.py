import base64
from Crypto.Cipher import AES


class SimpleEncrytionUtils(object):

    def __init__(self, key):
        self.bs = 16
        # self.key = hashlib.sha256(key.encode()).digest()
        self.key = key

    def encrypt(self, raw):
        raw = self._pad(raw)
        # iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key.encode("utf-8"), AES.MODE_ECB)
        return base64.b64encode(cipher.encrypt(raw.encode("utf-8")))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        cipher = AES.new(self.key.encode("utf-8"), AES.MODE_ECB)
        print(cipher.decrypt(enc[AES.block_size:]))
        return self._unpad(cipher.decrypt(enc)).decode("utf-8")

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s) - 1:])]
