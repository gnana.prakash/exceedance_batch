# -----------------Start of Import statements------------------------ #
import json

from elasticsearch import Elasticsearch

from scripts.logging.Logging import logger as log

# -----------------end of Import statements------------------------ #

final_network_site_mapping = {}


def _decode_list(data):
    """
            This function is to _decode_list
            :param data:
            :return rv:
    """
    rv = data
    return rv


def _decode_dict(data):
    """
        This function is to _decode_dict
        :param data:
        :return rv:
    """
    rv = data
    return rv


class ESUtility(object):
    """
        This class is for ESUtility
    """

    def __init__(self, es_url):
        """
            Initializer
            :param es_url:
        """
        try:
            self.__ES_URL__ = es_url
            self.__ES_OBJ__ = Elasticsearch(self.__ES_URL__)
        except Exception as e:
            log.exception(e)

    def update_elastic_search(self, index_name, index_type, index_id, final_json):
        """
            This function is for update_elastic_search
            :param index_name:
            :param index_type:
            :param index_id:
            :param final_json:
            :return:
        """
        if index_id == "":
            es_response = self.__ES_OBJ__.index(index=index_name, doc_type=index_type, body=final_json)
        else:
            es_response = self.__ES_OBJ__.index(index=index_name, doc_type=index_type, id=index_id, body=final_json)
        json_es_response_object = json.loads(json.dumps(es_response), object_hook=_decode_dict)
        return json_es_response_object["_id"]

    def query_elastic_search(self, index_name, index_type, size=10000000):
        """
            This function is to query_elastic_search
            :param index_name:
            :param index_type:
            :param size:
            :return:
        """
        try:
            es_response = self.__ES_OBJ__.search(index=index_name, doc_type=index_type, size=size, timeout=60)
        except Exception as e:
            log.exception("Index not found in Elastic Search. " + "Index -->" + index_name + " index_type-->" +
                          index_type + str(e))
            es_response = {}
        json_es_response_object = json.loads(json.dumps(es_response), object_hook=_decode_dict)
        return json_es_response_object

    def query_elastic_search_by_id(self, index_name, index_type, index_id):
        """
            This Function is to query_elastic_search_by_id
            :param index_name:
            :param index_type:
            :param index_id:
            :return:
        """
        try:
            es_response = self.__ES_OBJ__.get(index=index_name, doc_type=index_type, id=index_id)['_source']
        except Exception as e:
            log.exception("Index not found in Elastic Search. " + "Index -->" + index_name + " index_type-->" +
                          index_type + " index_id-->" + index_id + str(e))
            es_response = {}
        json_es_response_object = json.loads(json.dumps(es_response), object_hook=_decode_dict)
        return json_es_response_object

    def query_elastic_search_by_body(self, index_name, index_type, body_query, size=1000000):
        """
            This Function is to query_elastic_search_by_body
            :param index_name:
            :param index_type:
            :param body_query:
            :param size:
            :return:
        """
        try:
            es_response = self.__ES_OBJ__.search(index=index_name, doc_type=index_type, body=body_query, size=size)
        except Exception as e:
            log.exception("Index not found in Elastic Search. " + "Index -->" + index_name + " index_type-->"
                          + index_type + str(e))
            log.exception("Exception occurred in queryElasticSearchByBody() : ")
            es_response = {}
        json_es_response_object = json.loads(json.dumps(es_response), object_hook=_decode_dict)
        return json_es_response_object

    def count_elastic_search_by_body(self, index_name, index_type, body_query, size=1000000):
        """
            This Function is to count_elastic_search_by_body
            :param index_name:
            :param index_type:
            :param body_query:
            :param size:
            :return:
        """
        total_records = 0
        try:
            es_response = self.__ES_OBJ__.search(index=index_name, doc_type=index_type, body=body_query, size=size)
            json_es_response_object = json.loads(json.dumps(es_response), object_hook=_decode_dict)
            total_records = json_es_response_object["hits"]["total"]
            return total_records
        except Exception as e:
            log.exception("Index not found in Elastic Search. " + "Index -->" + index_name + " index_type-->"
                          + index_type + str(e))
            log.exception("Exception occurred in countElasticSearchByBody() : ")
        return total_records

    def delete_elastic_serarch_by_id(self, index_name, index_type, index_id):
        """
            This Function is for delete_elastic_serarch_by_id
            :param index_name:
            :param index_type:
            :param index_id:
            :return:
        """
        es_response = self.__ES_OBJ__.delete(index=index_name, doc_type=index_type, id=index_id, ignore=[400, 404])
        json_es_response_object = json.loads(json.dumps(es_response), object_hook=_decode_dict)
        return json_es_response_object

    @staticmethod
    def fetch_records_from_es_json(json_obj):
        """
            This function is for fetch_records_from_es_json
            :param json_obj:
            :return:
        """
        record_list = []
        try:
            temp_json_obj = json_obj["hits"]["hits"]
            if len(temp_json_obj):
                for record in temp_json_obj:
                    record_list.append(record["_source"])
        except Exception as e:
            log.exception("Exception in fetchRecordsFromESJson" + str(e))
        return record_list

    @staticmethod
    def fetch_buckets_from_es_json(json_obj, query_name):
        """
            This Function is for fetch_buckets_from_es_json
            :param json_obj:
            :param query_name:
            :return:
        """
        temp_json_obj = json_obj["aggregations"][query_name]["buckets"]
        record_list = []
        for record in temp_json_obj:
            record_list.append(record["key"].title())
        return record_list

    # Getting the basic site information for the Site Dashboard Loading. THe method is in
    # Metastore,Dashboard, and Status.
    def get_site_details(self):
        """
            This function is to get_site_details
            :return:
        """
        es_query = {"_source": "siteInfo"}
        tempbody_content = self.query_elastic_search_by_body("tnpcb", "site", es_query)
        site_list_json = self.fetch_records_from_es_json(tempbody_content)
        es_query = {"_source": "siteId"}
        tempbody_content = self.query_elastic_search_by_body("tnpcb", "mobile_site", es_query)
        mobile_site_list_json = self.fetch_records_from_es_json(tempbody_content)
        mobile_site_list = []
        for mobile_site in mobile_site_list_json:
            mobile_site_list.append(mobile_site["siteId"])
        final_site_list = []
        for site in site_list_json:
            try:
                temp_site_info = {}
                if "siteInfo" in site:
                    site_info = site["siteInfo"]
                    site_id = site_info["siteId"]
                    temp_site_info["siteName"] = site_info["siteName"]
                    if "siteLabel" in site_info:
                        temp_site_info["siteLabel"] = site_info["siteLabel"]
                    else:
                        temp_site_info["siteLabel"] = " "
                    temp_site_info["isMobileSite"] = False
                    if site_id in mobile_site_list:
                        temp_site_info["isMobileSite"] = True
                    temp_site_info["industry"] = site_info["industry"]
                    temp_site_info["city"] = site_info["city"]
                    temp_site_info["district"] = site_info["district"]
                    temp_site_info["state"] = site_info["state"]
                    temp_site_info["siteId"] = site_info["siteId"]
                    final_site_list.append(temp_site_info)
            except Exception as e:
                log.exception("Exception caught in getSiteDetails for siteId-->" + site_id + str(e))
        return final_site_list

    def network_iterator(self, d, parent_network_id, parent_network_name):
        """
            This function is for network_iterator
            :param d:
            :param parent_network_id:
            :param parent_network_name:
            :return:
        """
        temp_network_id = ""
        if "networkId" in d:
            temp_network_id = str(d["networkId"])
            if parent_network_id != "":
                new_network_id = parent_network_id + "|" + temp_network_id
            else:
                new_network_id = temp_network_id
        else:
            new_network_id = parent_network_id
        temp_network_name = ""
        if "siteId" not in d and "networkName" in d:
            temp_network_name = d["networkName"]
            if parent_network_name != "":
                new_network_name = parent_network_name + "|" + temp_network_name
            else:
                new_network_name = temp_network_name
        else:
            new_network_name = parent_network_name
        if "siteId" in d:
            if new_network_id not in final_network_site_mapping:
                final_network_site_mapping[new_network_id] = {}
                final_network_site_mapping[new_network_id]["networkName"] = new_network_name
                final_network_site_mapping[new_network_id]["siteIdList"] = {}
                final_network_site_mapping[new_network_id]["siteIdList"][d["siteId"]] = d["siteName"]
                final_network_site_mapping[parent_network_id]["siteIdList"][d["siteId"]] = d["siteName"]
            elif d["siteId"] not in final_network_site_mapping[new_network_id]["siteIdList"]:
                final_network_site_mapping[new_network_id]["siteIdList"][d["siteId"]] = d["siteName"]
                final_network_site_mapping[parent_network_id]["siteIdList"][d["siteId"]] = d["siteName"]
        for k, v in d.iteritems():
            if isinstance(v, dict):
                self.network_iterator(v, new_network_id, new_network_name)
            elif isinstance(v, list):
                for item in v:
                    self.network_iterator(item, new_network_id, new_network_name)

    def fetch_network_hierarchy(self, body_content):
        """
            This function is for fetch_network_hierarchy
            :param body_content:
            :return:
        """
        global final_network_site_mapping
        final_network_site_mapping = {}
        try:
            for network in body_content:
                self.network_iterator(network, "", "")
        except Exception as e:
            log.exception("Exception occurred in networkIterator" + str(e))
        final_json = {}
        try:
            for network_key in final_network_site_mapping:
                temp_network_list = network_key.split("|")
                network_name_list = final_network_site_mapping[network_key]["networkName"].split("|")
                prev_key = ""
                prev_network_name = ""
                i_count = 0
                for network in temp_network_list:
                    if prev_key == "":
                        prev_key = network
                        prev_network_name = network_name_list[i_count]
                    else:
                        prev_key = prev_key + "-->" + network
                        prev_network_name = prev_network_name + "-->" + network_name_list[i_count]
                    if prev_key not in final_json:
                        final_json[prev_key] = {}
                        final_json[prev_key]["networkName"] = prev_network_name
                        final_json[prev_key]["siteIdList"] = {}
                    for site_id in final_network_site_mapping[network_key]["siteIdList"]:
                        if site_id not in final_json[prev_key]["siteIdList"]:
                            final_json[prev_key]["siteIdList"][site_id] = final_network_site_mapping[network_key][
                                "siteIdList"][site_id]
                    i_count = i_count + 1
        except Exception as e:
            log.exception("Exception occurred in fetchNetworkHierarchy" + str(e))
        return final_json

    def fetch_site_to_regulator_mapping(self, site_id, master_nwid=""):
        """
            This function is for fetch_site_to_regulator_mapping
            :param site_id:
            :param master_nwid:
            :return:
        """
        body_content = []
        for network_id in master_nwid.split(","):
            network_json = self.query_elastic_search_by_id("tnpcb", "network", network_id)
            body_content.append(network_json)
        network_key_value_map = self.fetch_network_hierarchy(body_content)
        tempbody_content = self.query_elastic_search("tnpcb", "userSetup")
        user_list = self.fetch_records_from_es_json(tempbody_content)
        regulator_list = []
        final_networks_list = []
        for network in network_key_value_map:
            if site_id in network_key_value_map[network]["siteIdList"] and network not in final_networks_list:
                final_networks_list.append(network)
        for user in user_list:
            if user["userRole"] in ["Regulator", "SuperRegulator", "Vendor"] and user["networkAccess"] is not None:
                for network in user["networkAccess"]:
                    if network in final_networks_list:
                        user.pop("password", None)
                        regulator_list.append(user)
                        break
        return regulator_list

    def get_site_user_mapping(self, user_id, user_type):
        """
            This function is to get_site_user_mapping
            :param user_id:
            :param user_type:
            :return:
        """
        site_id_list = []
        try:
            site_details = {}
            if user_type == "Admin" or user_type == "CallCenter":
                site_details = self.get_site_details()
                for site_id in site_details:
                    if site_id["siteId"] not in site_id_list:
                        site_id_list.append(site_id["siteId"])
                return site_id_list
            elif user_type == "Regulator" or user_type == "SuperRegulator":
                user_details = self.query_elastic_search_by_id("tnpcb", "userSetup", user_id)
                network_id_list = user_details["networkAccess"]
                tempbody_content = self.query_elastic_search("tnpcb", "network")
                body_content = self.fetch_records_from_es_json(tempbody_content)
                network_key_value_map = self.fetch_network_hierarchy(body_content)
                if network_id_list is not None:
                    for network in network_id_list:
                        if network in network_key_value_map:
                            for site in network_key_value_map[network]["siteIdList"]:
                                if site not in site_id_list:
                                    site_id_list.append(site)
                return site_id_list
            elif user_type == "Client":
                user_details = self.query_elastic_search_by_id("tnpcb", "userSetup", user_id)
                if user_details["userAccess"] not in site_id_list:
                    site_id_list.append(user_details["userAccess"])
                return site_id_list
        except Exception as e:
            log.exception("Exception caught when requesting parameter values" + str(e))
        return site_id_list

    def fetch_user_to_parameters(self, user_id, site_id):
        """
            This function is to fetch_user_to_parameters
            :param user_id:
            :param site_id:
            :return:
        """
        monitoring_id_list = []
        user_tags = []
        user_role = ""
        try:
            user_setup_json = self.query_elastic_search_by_id("tnpcb", "userSetup", user_id)
            if "tags" in user_setup_json and user_setup_json["tags"] != "":
                user_tags = user_setup_json["tags"].split(",")
            if "userRole" in user_setup_json and user_setup_json["userRole"] != "":
                user_role = user_setup_json["userRole"]
            es_qurey = {"_source": ["acquisitionSetup.monitoringDetails.monitoringId",
                                    "acquisitionSetup.monitoringDetails.tags"],
                        "query": {"bool": {"must": [{"match": {"siteInfo.siteId": site_id}}]}}}
            site_setup_json = self.query_elastic_search_by_body("tnpcb", "site", es_qurey)
            site_json = self.fetch_records_from_es_json(site_setup_json)
            for monitoring_unit in site_json[0]["acquisitionSetup"]["monitoringDetails"]:
                if not user_tags:
                    monitoring_id_list.append(monitoring_unit["monitoringId"])
                    continue
                if "tags" in monitoring_unit and monitoring_unit["tags"] != "":
                    monitor_tags = monitoring_unit["tags"].replace(" ", "").split(",")
                    for user_tag in user_tags:
                        user_tag = user_tag.replace(" ", "")
                        if user_tag in monitor_tags and monitoring_unit["monitoringId"] not in monitoring_id_list:
                            monitoring_id_list.append(monitoring_unit["monitoringId"])
                            break
                elif monitoring_unit["monitoringId"] not in monitoring_id_list:
                    monitoring_id_list.append(monitoring_unit["monitoringId"])
                if user_role == "Admin" and monitoring_unit["monitoringId"] not in monitoring_id_list:
                    monitoring_id_list.append(monitoring_unit["monitoringId"])
        except Exception as e:
            log.exception("Exception in fetchUserToParameters" + str(e))
        return monitoring_id_list
