from configparser import ConfigParser

parser = ConfigParser()

parser.read("conf/configurations.conf")

BASE_PATH = parser.get('LOG', 'basepath')
LOG_LEVEL = parser.get('LOG', 'log_level')
FILE_NAME = parser.get('LOG', 'file_name')


__ES_URL__ = parser.get("data_base_details", "es_url")
__KAIROS_URL__ = parser.get("data_base_details", "kairos_url")

receivers_mail = parser.get("mail_details", "receivers_mail")

csv_file_path = parser.get("files_paths", "csv_file_path")
excel_file_path = parser.get("files_paths", "excel_file_path")

FROM_ADDRESS = parser.get("email_settings", "from_address")
PASSWORD = parser.get("email_settings", "password")
MAIL_MESSAGE = parser.get("email_settings", "mail_message")
MAIL_MESSAGE1 = parser.get("email_settings", "mail_message1")
SENDER_NAME = parser.get("email_settings", "sender_name")
SALUTATION_MESSAGE = parser.get("email_settings", "salutation_message")
REGARDS_MESSAGE = parser.get("email_settings", "regards_message")
FOOTER_MESSAGE = parser.get("email_settings", "footer_message")
smtp_address = parser.get("email_settings", "smtp_address")
smtp_port = parser.get("email_settings", "smtp_port")