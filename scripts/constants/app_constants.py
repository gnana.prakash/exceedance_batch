class ELASTIC_SEARCH:
    ES_SITE_INDEX = "tnpcb"
    ES_SITE_DOC_TYPE = "site"


SiteData = "siteData"
SiteID = "siteId"
METRIC_NAME = "glens_milis.upload.valid"
FREQUENCY = "15-minutes"
RAW = "U"

ADD_JSON = [
    {
        "name": "tag",
        "tags": [
            "analyser_id",
            "monitoring_type",
            "monitoring_id",
            "parameter_id",
            "quality"
        ]
    }
]