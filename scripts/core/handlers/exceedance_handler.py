"""
AUTHOR: GNANA PRAKASH INT-561
"""
from scripts.logging.Logging import logger as log
from scripts.constants import app_configurations, app_constants
from scripts.utils.es_utility import ESUtility
from scripts.utils.kairosdb_json import KairosDBJson
from scripts.utils.kairosdbutility import KairosDButlity
from datetime import datetime, timedelta
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
import smtplib


def send_email(to_address_final, file_name, site_name):
    try:
        log.info("Start of send email")
        today = datetime.today()
        d1 = today.strftime("%Y-%m-%d")
        from_address = app_configurations.FROM_ADDRESS
        password = app_configurations.PASSWORD
        subject = "OSPCB EXCEEDANCE REPORT" + '|' + site_name
        mail_message = app_configurations.MAIL_MESSAGE
        header = 'To:' + str(",".join(to_address_final)) + '\n' + 'From: ' + \
                 app_configurations.SENDER_NAME + '\n' + 'Subject:' + subject + '\n' + 'Date:' + d1
        message = MIMEMultipart()
        message.attach(
            MIMEText(app_configurations.SALUTATION_MESSAGE + '\n\n' + mail_message
                     + '\n\n' + app_configurations.REGARDS_MESSAGE.replace("+", "\n")
                     + '\n' + app_configurations.FOOTER_MESSAGE + '\n'))
        part_pd = MIMEBase('application', 'octet-stream')
        part_pd.set_payload(open(file_name, 'rb').read())
        encoders.encode_base64(part_pd)
        part_pd.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(str(file_name)))
        message.attach(part_pd)

        final_msg = header + '\n' + message.as_string()
        smtp_server = smtplib.SMTP(app_configurations.smtp_address, app_configurations.smtp_port)
        smtp_server.ehlo()
        smtp_server.starttls()
        smtp_server.login(from_address, password)
        smtp_server.sendmail(from_address, to_address_final, final_msg)
        log.info('Email Sent Successfully')
        smtp_server.close()
        log.info("End of send email  method")
        return True
    except Exception as e:
        log.info('Email not send successfully')
        log.error("Exception in send email excel method---->" + str(e))
        log.info("End of send email  method")
        return False


# def mail_services(final_list, receivers_list, site_name):
#     try:
#         log.info("starting mail services")
#         columns_list = []
#         for each_dict in final_list:
#             for columns in each_dict:
#                 columns_list.append(columns)
#         csv_path = str(app_configurations.csv_file_path + site_name + ".csv")
#         with open(csv_path, 'w') as outfile:
#             writer = DictWriter(outfile, columns_list)
#             writer.writeheader()
#             writer.writerows(final_list)
#         excel_path = str(app_configurations.excel_file_path + site_name + ".xlsx")
#         spreadsheet = pd.read_csv(csv_path)
#         final_excel = pd.ExcelWriter(excel_path)
#         spreadsheet.to_excel(final_excel, index=False)
#         final_excel.save()
#     except Exception as e:
#         log.error("error in sending mails {}", str(e))


class Exceedance(object):
    def __init__(self):
        log.info("initiating Database access")
        self.es_url = ESUtility(app_configurations.__ES_URL__)
        self.kairosdb_url = KairosDButlity(app_configurations.__KAIROS_URL__)
        self.avg_json = KairosDBJson()

    def get_exceedance(self):
        try:
            current_date = datetime.now()
            current_string = datetime.strftime(current_date, "%Y-%m-%d") + " 00:00:00"
            current_ptime = datetime.strptime(current_string, "%Y-%m-%d %H:%M:%S")
            to_time = int(current_ptime.timestamp()) * 1000
            yesterday = current_date - timedelta(days=1)
            yes_string = datetime.strftime(yesterday, "%Y-%m-%d") + " 00:00:00"
            yes_ptime = datetime.strptime(yes_string, "%Y-%m-%d %H:%M:%S")
            from_time = int(yes_ptime.timestamp()) * 1000
            site_data_es = self.es_url.query_elastic_search(app_constants.ELASTIC_SEARCH.ES_SITE_INDEX,
                                                            app_constants.ELASTIC_SEARCH.ES_SITE_DOC_TYPE)
            site_data = self.es_url.fetch_records_from_es_json(site_data_es)
            for each_site in site_data:
                site_details = ["1"]
                final_list = []
                site_details.append(each_site["siteInfo"]["siteName"])
                site_details.append(each_site["siteInfo"]["industry"])
                site_details.append(each_site["siteInfo"]["city"])
                site_details.append(each_site["siteInfo"]["district"])
                site_id = each_site["siteInfo"]["siteId"]
                metric_json = self.avg_json.avg_data_json(metric_name=app_constants.METRIC_NAME,
                                                          cal_avg=app_constants.FREQUENCY,
                                                          quality_code=app_constants.RAW, site_id=site_id)
                del metric_json[0]["aggregators"][0]["align_sampling"]
                metric_json[0]["aggregators"][0]["align_start_time"] = "true"
                metric_json[0]["group_by"] = app_constants.ADD_JSON
                data = self.kairosdb_url.fetch_data(metric_json=metric_json, from_time=from_time, to_time=to_time)
                results_list = data["queries"][0]["results"]
                for each_dict in results_list:
                    kairos_key = ""
                    final_dict = dict()
                    if each_dict["tags"]:
                        kairos_key = each_dict["tags"]["monitoring_type"][0] + "." + \
                                     each_dict["tags"]["monitoring_id"][0] + "." + \
                                     each_dict["tags"]["analyser_id"][0] + "." + \
                                     each_dict["tags"]["parameter_id"][0]
                    for each_param in each_site:
                        if each_param == "sensorSetup":
                            monitoring_data = each_site[each_param]["subvalues"]
                            for each_type in monitoring_data:
                                if monitoring_data[each_type]:
                                    monitoring_list = monitoring_data[each_type]
                                    for each_dictionary in monitoring_list:
                                        monitoring_id = each_dictionary["monitoringId"]
                                        analyser_list = each_dictionary["analyser"]
                                        for each_analyser in analyser_list:
                                            analyser_id = each_analyser["key"]
                                            parameter_list = each_analyser["parameterDetails"]
                                            for each_parameter in parameter_list:
                                                parameter_name = each_parameter["value"]
                                                parameter_id = each_parameter["key"]
                                                es_key = each_type + "." + monitoring_id + "." + analyser_id \
                                                         + "." + parameter_id
                                                if kairos_key == es_key:
                                                    exceedance_count = 0
                                                    monitoring_label = ""
                                                    if each_parameter["alarmsMinThreshold"]["rangeValue"] != "" and \
                                                            each_parameter["alarmsMaxThreshold"]["rangeValue"] != "":
                                                        min_threshold = float(each_parameter["alarmsMinThreshold"]
                                                                                  ["rangeValue"])
                                                        max_threshold = float(each_parameter["alarmsMaxThreshold"]
                                                                                  ["rangeValue"])
                                                        if each_dict["values"]:
                                                            values_list = each_dict["values"]
                                                            for each_value_list in values_list:
                                                                value = round(each_value_list[1], 2)
                                                                if value < min_threshold or value > max_threshold:
                                                                    exceedance_count += 1
                                                        if "monitoringDetails" in each_site["acquisitionSetup"]:
                                                            if each_site["acquisitionSetup"]["monitoringDetails"]:
                                                                monitoring_details = each_site["acquisitionSetup"]["monitoringDetails"]
                                                                for each_monitoring in monitoring_details:
                                                                    mon_type = each_monitoring["monitoringType"]
                                                                    mon_id = each_monitoring["monitoringId"]
                                                                    dup_key = mon_type + "." + mon_id
                                                                    if dup_key in kairos_key:
                                                                        monitoring_label = each_monitoring[
                                                                            "monitoringLabel"]
                                                        key = monitoring_label + "-" + parameter_name
                                                        if exceedance_count != 0:
                                                            final_dict[key] = exceedance_count
                                                            final_list.append(final_dict)
                if final_list:
                    receivers_list = []
                    site_info = each_site["siteInfo"]
                    if "primaryContact" in site_info:
                        receivers_list.append(site_info["primaryContact"]["contactMail"])
                    if "secondaryContact" in site_info:
                        receivers_list.append(site_info["secondaryContact"]["contactMail"])
                    receivers_list.append(app_configurations.receivers_mail)
                    site_name = site_info["siteName"]
                    file_name = self.create_excel(final_list, site_details)
                    send_email(receivers_list, file_name, site_name)
        except Exception as e:
            log.error(str(e))

    @staticmethod
    def create_excel(final_list, site_details):
        try:
            from pandas.tests.io.excel.test_xlsxwriter import xlsxwriter
            from xlsxwriter.utility import xl_rowcol_to_cell
            from email import encoders
            header_list = ["Sl No", "Industry Name", "Industry Category", "City", "District"]
            for each in final_list:
                head = list(each.keys())
                data = list(each.values())
                header_list.extend(head)
                site_details.extend(data)
            file_name = str(app_configurations.excel_file_path + site_details[1] + ".xlsx")
            workbook = xlsxwriter.Workbook(file_name)
            report_sheet = workbook.add_worksheet("Sheet1")
            value_format = workbook.add_format(
                {'align': 'center', 'text_wrap': '1', 'valign': 'vcenter', 'border': '1', })
            workbook.add_format({'align': 'center'})
            col_end = xl_rowcol_to_cell(1, 15)
            report_sheet.set_column('A:' + col_end, 25)
            title_format = workbook.add_format({
                'bold': True,
                'align': 'center',
                'valign': 'vcenter',
                'text_wrap': '1',
                'border': '1',
                'fg_color': '#969F83',
            })
            title_format.set_border(1)
            report_sheet.set_column(1, 0, 50)
            report_sheet.set_row(1, 50)
            row = 0
            col = 0
            for each in header_list:
                report_sheet.write(row, col, each, title_format)
                col = col + 1

            row = 1
            col = 0
            for value in site_details:
                report_sheet.write(row, col, value, value_format)
                col = col + 1
            workbook.close()
            return file_name
        except Exception as e:
            log.error(" Error in creating excel {}", str(e))
