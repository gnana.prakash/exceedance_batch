from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.cron import CronTrigger
from scripts.core.handlers.exceedance_handler import Exceedance
from scripts.logging.Logging import logger as log

scheduler = BlockingScheduler(daemon=True)
if __name__ == '__main__':
    exceedance_obj = Exceedance()
    log.info("Adding New Scheduler")
    exceedance_obj.get_exceedance()
    # scheduler.add_job(exceedance_obj.get_exceedance, CronTrigger.from_crontab('*/1 * * * *'), max_instances=10,
    #                   misfire_grace_time=10)
    # while True:
    #     try:
    #         scheduler.start()
    #         log.info("Waiting for 1 minute")
    #     except Exception as e:
    #         log.error("Exception caught when updating site status : %s", str(e), exc_info=True)
