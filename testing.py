from pandas.tests.io.excel.test_xlsxwriter import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from email import encoders
site = ['1','Water Quality Monitoring Station - Gostani river sample before confluence with Delta paper mill effluents', 'NWMP', 'Palakoderu', 'West Godavari']
final = [{'Public-BOD': 96}, {"jaga": 100}, {"nomi": 150}]
header_list = ["Sl No", "Industry Name", "Industry Category", "City", "District"]
for each in final:
    head = list(each.keys())
    data = list(each.values())
    header_list.extend(head)
    site.extend(data)

workbook = xlsxwriter.Workbook("testreport.xlsx")
report_sheet = workbook.add_worksheet("Sheet1")
value_format = workbook.add_format(
    {'align': 'center', 'text_wrap': '1', 'valign': 'vcenter', 'border': '1', })
workbook.add_format({'align': 'center'})
# report_sheet.freeze_panes(3, 0)
col_end = xl_rowcol_to_cell(1, 15)
report_sheet.set_column('A:' + col_end, 25)
# workbook.add_format({'color': 'blue', 'underline': 1})
title_format = workbook.add_format({
    'bold': True,
    'align': 'center',
    'valign': 'vcenter',
    'text_wrap': '1',
    'border': '1',
})
title_format.set_border(1)
header_format = workbook.add_format({
    'bold': True,
    'align': 'center',
    'valign': 'vcenter',
    'text_wrap': '1',
    'font_size': '14',
    'fg_color': '#D7E4BC',
})

# header_format.set_top()
# header_format.set_left()
# header_format.set_right()
# le = len(final_list[0])
# report_sheet.merge_range(0, 0, 0, le - 1, site_name, header_format)
# report_sheet.merge_range(1, 0, 1, le - 1, "From Date" + from_time + " To Date " + to_time, title_format)
# report_sheet.merge_range(2, 0, 2, le - 1, "Report Name: Custom Report", title_format)
report_sheet.set_column(1, 2, 50)
report_sheet.set_row(4,50)
row = 3
col = 0
for each in header_list:
    report_sheet.write(row, col, each, title_format)
    col = col + 1

row = 4
col = 0
for value in site:
    report_sheet.write(row, col, value, value_format)
    col = col + 1
workbook.close()